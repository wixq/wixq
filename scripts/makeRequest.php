<?php

  include "../../creds.php";

  $servername = "localhost";

  // Create connection
  $conn = new mysqli($servername, $username, $password);

  // Check connection
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  } 

  $stmt = $conn->prepare("INSERT INTO dbo.tblRequest (requestedBy, songTitle, songArtist) VALUES (?, ?, ?)");
  
  $stmt->bind_param('sss', $requester, $song, $artist);
  $requester = $_POST["requester"];
  $song = $_POST["song"];
  $artist = $_POST["artist"];
  
  $stmt->execute();

  $conn->close();

?>