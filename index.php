<!DOCTYPE html>
<html lang="en">
<head>
	<title>WIXQ guerilla site</title>
	<!-- Jquery & bootstrap -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
	<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css"/>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

	<!-- neat lil font -->
	<link href='http://fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	
	<!-- local scripts/styles -->
	<script src="scripts/konami.js" type="text/javascript"></script>
	<script src="scripts/index.js"></script>
	<link rel="stylesheet" type="text/css" href="stylesheets/index.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!--favicon-->
	<link rel="icon" 
      type="image/png" 
      href="http://i.imgur.com/YoqP0wh.jpg">
</head>

<body>

	<div id="dvBodyWrapper">

		<!-- HEADER -->
		<a id="dvLogoHeader"> 
			<div>
				<!--<img src="http://www.millersville.edu/csil/Images/Logos/WIXQ.png" />-->
				<img src="http://i.imgur.com/FiWUd2j.png" />
			</div>
		</a>
		<div id="dvHeaderBottom"></div>
		
		<!-- END HEADER -->
		<div id="dvBody" class="main-color">
			<div id="dvPlayerBackground"></div>

				<div id="dvAbout">
					<p>WIXQ is your number one music source</p>
					<p>Broadcasting in the Lancaster/Millersville area</p>
					<p>Call us at 717-871-7075</p>
				</div>

				<div id="dvFancyPlayer">
					<a id="playerText">listen live!</a>
					<a id="btnPlayPause"><span class="butt glyphicon glyphicon-play"></span></a>
					<a id="btnMute" class="hidden"><span class="butt glyphicon glyphicon-volume-up"></span></a>
				</div>

				<div id="dvRequestLabel" class="sectionHeader"> 
					make a request!
					<hr class="hr20" />
				</div>

				<div class="hide requestConfirm">
					Thanks for the request! Keep listening and maybe you'll hear it!
				</div>

				<div id="dvRequestForm" class="form-group">
					<form class="php-json-submit" action="scripts/makeRequest.php" method="post">
							<div><input class="requestForm form-control" type="text" id="song" name="song" placeholder="song name"></div>
							<div><input class="requestForm form-control" type="text" id="artist" name="artist" placeholder="artist"></div>
							<div><input class="requestForm form-control" type="text" id="requester" name="requester" placeholder="requested by"></div>
							<div><input class="btn" type="submit" id="btnSubmitRequest" value="submit"></div>
					</form>
				</div>

				<div class="clearfix"></div>

				<div id="dvPGridLabel" class="sectionHeader">
					Video stream!
					<hr class="hr20"/>
				</div>
		</div>

		<div class="main-color">
			<div id="dvProgramGrid">
				<!-- NEAT -->
				<img class="not-watching" src="http://i.imgur.com/jSTMtGM.png" />
			</div>

			<div id="dvFooter">
				<div class="dvOrangeFooter">
					<h1>here be the footer</h1>
				</div>

				<div class="dvGreyFooter"></div>
			</div>
		</div>

	</div>

	<audio id="audioStream">
		  <source src="http://166.66.176.252:8004/;?icy=http" type="audio/mpeg" />
		</audio>

	<audio id="womboCombo">
		<source src="http://www.myinstants.com/media/sounds/wombo-combo.mp3" />
	    your browser don't support the audio stuff
	</audio>

</body>
</html>